const countries = [
  { name: "Country 1", population: 50 },
  { name: "Country 2", population: 25 },
  { name: "Country 3", population: 10 },
  { name: "Country 4", population: 5 },
  { name: "Country 5", population: 3 },
  { name: "Country 6", population: 2 },
  { name: "Country 7", population: 2 },
  { name: "Country 8", population: 1 },
  { name: "Country 9", population: 1 },
  { name: "Country 10", population: 1 },
];

const getCountries = (filter) =>
  countries.filter(({ name }) =>
    name.toLowerCase().includes(filter.toLowerCase())
  );

const getTotalPopulation = () => 100;

module.exports = { getCountries, getTotalPopulation };
