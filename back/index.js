const http = require("http");
const dotenv = require("dotenv");
const { handleGetCountries } = require("./src/controllers/countryController");
const { OK, NOT_FOUND } = require("./src/constants");

dotenv.config({ path: ".env" });

const {
  env: { PORT, HOST },
} = process;

const server = http.createServer(
  { requireHostHeader: false },
  ({ method, url }, res) => {
    const parsedUrl = url.slice(0, 2);

    const responseHeaders = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
    };

    if (method === "OPTIONS" && ["/", "/?"].includes(parsedUrl)) {
      res.writeHead(OK, "Ok", responseHeaders);

      return res.end();
    }

    if (method !== "GET" || !["/", "/?"].includes(parsedUrl)) {
      res.writeHead(NOT_FOUND, {
        "Content-Type": "application/json",
        ...responseHeaders,
      });

      return res.end();
    }

    handleGetCountries(url.split("?")[1]).then(({ code, data }) => {
      res.writeHead(code, {
        "Content-Type": "application/json",
        ...responseHeaders,
      });
      res.end(JSON.stringify(data));
    });
  }
);

server.listen(PORT, HOST, () => console.info("Server up at", PORT));
