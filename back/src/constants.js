// Codes
const NO_CONTENT = 204;
const OK = 200;
const NOT_FOUND = 404;
const INTERNAL_ERROR = 500;

const LIMIT = 5;

module.exports = { NO_CONTENT, OK, INTERNAL_ERROR, NOT_FOUND, LIMIT };
