const { NO_CONTENT, OK, INTERNAL_ERROR } = require("../constants");
const {
  getCountries,
  getTotalPopulation,
} = require("../services/countryService");

const getQueryParamValue = (queryParam) => {
  if (!queryParam?.length) return "";

  const queryParams = queryParam.split("&");

  for (let i = 0; i < queryParams.length; i++) {
    const currentQueryParam = queryParams[i];
    console.info({
      currentQueryParam,
    });

    if (currentQueryParam.slice(0, 8) === "country=") {
      return currentQueryParam.split("=")[1];
    }
  }

  return "";
};

const handleGetCountries = async (queryValues) => {
  try {
    const countryFilter = getQueryParamValue(queryValues);

    console.info("memerino", { countryFilter, queryValues })

    if (countryFilter.length < 3) return { code: NO_CONTENT };

    const [rawCountries, totalPopulation] = await Promise.all([
      getCountries(countryFilter),
      getTotalPopulation(),
    ]);

    const countries = rawCountries.map(({ population, ...rest }) => ({
      ...rest,
      population,
      populationPercent:
        totalPopulation &&
        ((Number(population) * 100) / totalPopulation).toFixed(2),
    }));

    return { code: OK, data: countries };
  } catch (error) {
    console.error("[countryController.handleGetCountries] Error:", error);

    return { code: INTERNAL_ERROR, data: "Error at countries get" };
  }
};

module.exports = { handleGetCountries };
