const countries = [
  { name: "Country 1", population: 500 },
  { name: "Country 2", population: 250 },
  { name: "Country 3", population: 100 },
  { name: "Country 4", population: 50 },
  { name: "Country 5", population: 30 },
  { name: "Country 6", population: 20 },
  { name: "Country 7", population: 20 },
  { name: "Country 8", population: 10 },
  { name: "Country 9", population: 10 },
  { name: "Country 10", population: 10 },
];

const getCountries = (filter) =>
  countries.filter(({ name }) =>
    name.toLowerCase().includes(filter.toLowerCase())
  ).slice(0, 5);

const getTotalPopulation = () => 1000;

module.exports = { getCountries, getTotalPopulation };
