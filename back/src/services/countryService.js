const { LIMIT } = require("../constants");
const { runQuery } = require("./postgresService");

const getCountries = async (nameFilter) => {
  const baseQuery = "SELECT * FROM countries c";
  const order = `ORDER BY c.population DESC LIMIT ${LIMIT};`;
  const filter = nameFilter ? ` WHERE name ilike '%${nameFilter}%'` : "";

  const result = await runQuery(`${baseQuery}${filter} ${order}`);

  return result.rows;
};

const getTotalPopulation = async () => {
  const query =
    "SELECT sum(c.population) as total_population FROM countries c;";

  const result = await runQuery(query);

  return result.rows?.[0]?.total_population;
};

module.exports = { getCountries, getTotalPopulation };
