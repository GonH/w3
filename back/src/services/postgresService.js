const { Client } = require("pg");
const dotenv = require("dotenv");

dotenv.config({ path: ".env" });

const {
  env: { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS },
} = process;

let connection;

const getClient = async () => {
  if (connection) return connection;

  const client = new Client({
    host: DB_HOST,
    port: DB_PORT,
    database: DB_NAME,
    user: DB_USER,
    password: DB_PASS,
  });

  await client.connect();
  connection = client;

  return connection;
};

const runQuery = async (query) => {
  const client = await getClient();

  return client.query(query);
};

module.exports = { getClient, runQuery };
