const {
  NO_CONTENT,
  INTERNAL_ERROR,
  OK,
  LIMIT,
} = require("../../../src/constants");
const {
  handleGetCountries,
} = require("../../../src/controllers/countryController");

jest.mock("../../../src/services/countryService.js");

describe("[handleGetCountries]", () => {
  test("Not sending a query param returns a 204 code and no data field", async () => {
    const { code, data } = await handleGetCountries("");

    expect(code).toBe(NO_CONTENT);
    expect(data).toBe(undefined);
  });

  test("Sending a query params without the country key returns a 204 code and no data field", async () => {
    const { code, data } = await handleGetCountries("key1=1&key2=2&key3=3");

    expect(code).toBe(NO_CONTENT);
    expect(data).toBe(undefined);
  });

  test("Sending the country key as query param with less than 3 characters returns a 204 code and no data field", async () => {
    const { code, data } = await handleGetCountries("country=in&key3=3");

    expect(code).toBe(NO_CONTENT);
    expect(data).toBe(undefined);
  });

  test("Sending a non string value with no length returns a 204 code and no data field", async () => {
    const { code, data } = await handleGetCountries(1);

    expect(code).toBe(NO_CONTENT);
    expect(data).toBe(undefined);
  });

  test("Sending a non string value with length returns a 500 code and an error message in data field", async () => {
    const { code, data } = await handleGetCountries([null]);

    expect(code).toBe(INTERNAL_ERROR);
    expect(typeof data).toBe("string");
  });

  test("A filter with the country key and more than 3 characters returns a 200 code", async () => {
    const { code } = await handleGetCountries("country=country");

    expect(code).toBe(OK);
  });

  test("Using a valid filter returns an array in the field data", async () => {
    const { data } = await handleGetCountries("country=country");

    expect(Array.isArray(data)).toBe(true);
  });

  test("Each record in the result has three fields, name, population and populationPercent", async () => {
    const { data } = await handleGetCountries("country=country");

    data.forEach(({ name, population, populationPercent }) => {
      expect(typeof name).toBe("string");
      expect(typeof population).toBe("number");
      expect(typeof populationPercent).toBe("string");
    });
  });

  // The total population is hardcoded in 1000 to simplify testing
  test("The field populationPercent has a string with the population divided by 10 and two decimals", async () => {
    const { data } = await handleGetCountries("country=country");

    data.forEach(({ population, populationPercent }) => {
      expect(`${population / 10}.00`).toBe(populationPercent);
    });
  });
});
