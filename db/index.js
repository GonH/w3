const { Pool } = require("pg");
const { getTop20CountriesByPopulationSeed } = require("./seeds");
const { getCountryTableCreationMigration } = require("./migrations");
const dotenv = require("dotenv");

dotenv.config({ path: ".env" });

const {
  env: { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS },
} = process;

const getQueries = () => [
  getCountryTableCreationMigration(),
  getTop20CountriesByPopulationSeed(),
];

const handleMigrationsAndSeeds = async () => {
  const client = new Pool({
    host: DB_HOST,
    port: DB_PORT,
    database: DB_NAME,
    user: DB_USER,
    password: DB_PASS,
  });
  await client.connect;

  try {
    await client.query("BEGIN");

    const queries = getQueries();

    for (let i = 0; i < queries.length; i++) await client.query(queries[i]);

    await client.query("COMMIT");
  } catch (error) {
    console.error("Error at migrations", error);

    await client.query("ROLLBACK");

    throw error;
  } finally {
    client.end();
  }
};

handleMigrationsAndSeeds();
