const getCountryTableCreationMigration = () => `
  CREATE TABLE IF NOT EXISTS countries (
    name varchar(255) UNIQUE,
    population BIGINT
  );
`;

module.exports = { getCountryTableCreationMigration };
