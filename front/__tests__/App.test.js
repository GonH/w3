import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import React from 'react';
import App from '../src/App';
import userEvent from '@testing-library/user-event'
// import apiCalls from "../src/apiCalls";

jest.mock('../src/apiCalls');

test('The component has two children, the filter and table', async () => {
  render(<App />)

  const app = screen.getByTestId('app');

  expect(app.childElementCount).toEqual(2);
});

test('Changing the input changes the amount of countries in the state', async () => {
  render(<App />)

  // const app = screen.getByTestId('app');

  const input = screen.getByRole("textbox");

  await fireEvent.input(input, { target: { value: "country" } });

  await userEvent.click(screen.getByRole("button"));

  expect(screen.getByText("Country 1")).toBeInTheDocument();
  expect(screen.getByText("Country 2")).toBeInTheDocument();
  expect(screen.getByText("Country 3")).toBeInTheDocument();
  expect(screen.getByText("Country 4")).toBeInTheDocument();
  expect(screen.getByText("Country 5")).toBeInTheDocument();

  await fireEvent.input(input, { target: { value: "country 1" } });

  await userEvent.click(screen.getByRole("button"));

  expect(screen.getByText("Country 1")).toBeInTheDocument();

  await fireEvent.input(input, { target: { value: "country 11" } });

  await userEvent.click(screen.getByRole("button"));

  expect(screen.queryByText("Country 1")).not.toBeInTheDocument();
});
