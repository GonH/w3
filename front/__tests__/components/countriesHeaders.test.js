import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import CountriesHeader from "../../src/components/countriesHeader";
import React from 'react';

// I know this is a sad test but since I havent made a test in ages I decided to test this
// hardcoded component to familiarize myself with the tools
test('Loads the header with 3 columns (name, population and population percent)', async () => {
  render(<CountriesHeader />)

  const header = screen.getByRole('rowgroup')

  // ASSERT
  // expect(screen.getByRole('rowgroup').childNodes).('hello there')
  expect(header.childElementCount).toEqual(3);
  expect(header.firstElementChild).toHaveTextContent("País");
  expect(screen.getByText("Población")).toBeDefined();
  expect(header.lastElementChild).toHaveTextContent("Porcentaje de habitantes (en base al total)");
});
