import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import React from 'react';
import CountriesRow from '../../src/components/countriesRow';

test('The component renders the params as is, except the percent, that gets an % added', async () => {
  render(<CountriesRow name="Country" population={3} populationPercent="3" />)

  expect(screen.getByText("Country")).toBeDefined();
  expect(screen.getByText("3")).toBeDefined();
  expect(screen.getByText("3 %")).toBeDefined();
});

test('The component renders populations greater or equal than with dots as the thousands dividing character', async () => {
  render(<CountriesRow name="Country" population={30000000} populationPercent="3" />)

  expect(screen.getByText("30.000.000")).toBeDefined();
});
