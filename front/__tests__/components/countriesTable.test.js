import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import React from 'react';
import CountriesTable from '../../src/components/countriesTable';

test('A message is rendered when no countries are sent', async () => {
  render(<CountriesTable countries={[]} />)

  expect(screen.getByText("No se encontraron resultados")).toBeDefined();
});

test('If more than one element is sent, the component has two children, the header and body', async () => {
  render(<CountriesTable countries={[{ country: "C", population: 1, populationPercent: 1 }]} />)

  const table = screen.getByRole('table');

  expect(table.childElementCount).toEqual(2);
  expect(table.firstElementChild.tagName).toEqual("THEAD");
  expect(table.lastElementChild.tagName).toEqual("TBODY");
});

test('The body has as many rows children as the param length sent', async () => {
  const countriesA = [{ country: "C", population: 1, populationPercent: 1 }];
  const countriesB = [{ country: "C", population: 2, populationPercent: 2 }, { country: "D", population: 1, populationPercent: 1 }]

  const { unmount } = render(<CountriesTable countries={countriesA} />)

  let table = screen.getByRole('table');

  expect(table.lastElementChild.childElementCount).toEqual(countriesA.length);

  unmount();

  render(<CountriesTable countries={countriesB} />)

  table = screen.getByRole('table');

  expect(table.lastElementChild.childElementCount).toEqual(countriesB.length);
});
