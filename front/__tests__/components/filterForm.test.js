import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import React from 'react';
import FilterForm from '../../src/components/filterForm';
import userEvent from '@testing-library/user-event'

test('Trying to submit without changing the input makes an alert appear', async () => {
  render(<FilterForm handleSubmit={() => { }} />)

  expect(screen.queryByText("El filtro debe tener mas de dos caracteres.")).toBeNull();

  await userEvent.click(screen.getByRole("button"));

  expect(screen.getByText("El filtro debe tener mas de dos caracteres.")).toBeDefined();
});

test('Trying to send a filter of less than 3 characters long makes an alert appear', async () => {
  render(<FilterForm handleSubmit={() => { }} />)

  expect(screen.queryByText("El filtro debe tener mas de dos caracteres.")).toBeNull();

  const input = screen.getByRole("textbox");

  fireEvent.input(input, { target: { value: "a" } });

  await userEvent.click(screen.getByRole("button"));

  expect(screen.getByText("El filtro debe tener mas de dos caracteres.")).toBeDefined();
});

test('Sending a valid filter avoids the alert', async () => {
  render(<FilterForm handleSubmit={() => { }} />)

  expect(screen.queryByText("El filtro debe tener mas de dos caracteres.")).toBeNull();

  const input = screen.getByRole("textbox");

  fireEvent.input(input, { target: { value: "aaaaa" } });

  await userEvent.click(screen.getByRole("button"));

  expect(screen.queryByText("El filtro debe tener mas de dos caracteres.")).toBeNull();
});
