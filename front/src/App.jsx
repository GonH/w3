import "./App.css";
import FilterForm from "./components/filterForm";
import React, { useState } from "react";
import CountriesTable from "./components/countriesTable";
import { searchCountries } from "./apiCalls";

const App = () => {
  const [countries, setCountries] = useState([]);

  return (
    <div data-testid="app" className="App">
      <FilterForm
        handleSubmit={(input) => searchCountries(input, setCountries)}
      />
      <CountriesTable countries={countries} />
    </div>
  );
};

export default App;
