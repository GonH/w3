const searchCountries = async (inputValue, setCountries) => {
  console.info("The meme", inputValue);

  switch (inputValue) {
    case "country": return setCountries([
      { name: "Country 1 ", population: 10, populationPercent: 10 },
      { name: "Country 2", population: 5, populationPercent: 5 },
      { name: "Country 3", population: 3, populationPercent: 3 },
      { name: "Country 4", population: 2, populationPercent: 2 },
      { name: "Country 5", population: 1, populationPercent: 1 },
    ]);
    case "country 1": return setCountries([{ name: "Country 1", population: 10, populationPercent: 10 }]);
    default: return setCountries([]);
  }
};

module.exports = {
  searchCountries
}