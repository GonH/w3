const searchCountries = async (inputValue, setCountries) => {
  const filter = inputValue ? `?country=${inputValue}` : "";
  const countriesList = await fetch(`${process.env.REACT_APP_BACK_URL}${filter}`, {
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  }).then((response) => response.json());

  setCountries(countriesList);
};

module.exports = {
  searchCountries
}