import React from 'react';

const CountriesHeader = () => (
  <thead className="countriesHead">
    <td>País</td>
    <td>Población</td>
    <td>Porcentaje de habitantes (en base al total)</td>
  </thead>
);

export default CountriesHeader;
