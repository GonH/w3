import React from "react";

const CountriesRow = ({ name, population, populationPercent }) => (
  <tr>
    <td>{name}</td>
    <td>{Number(population).toLocaleString().replace(/,/g, ".")}</td>
    <td>{populationPercent} %</td>
  </tr>
) ;

export default CountriesRow;
