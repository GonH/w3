import React from "react";
import CountriesHeader from "./countriesHeader";
import CountriesRow from "./countriesRow";
import "./countries.css";

const CountriesTable = ({ countries }) => {
  if (!countries.length) return <label>No se encontraron resultados</label>;

  return (
    <table className="countries">
      <CountriesHeader />
      <tbody data-testid="country-body">
        {countries.map((country) => (
          <CountriesRow key={country.key} {...country} />
        ))}
      </tbody>
    </table>
  );
};

export default CountriesTable;
