import React from "react";
import { useState } from "react";

const FilterForm = ({ handleSubmit }) => {
  const [input, setInput] = useState("");
  const [error, setError] = useState("");

  const useFilter = (event) => {
    event?.preventDefault();

    if (input.length < 3)
      return setError("El filtro debe tener mas de dos caracteres.");

    setError("");
    handleSubmit(input);
  };

  return (
    <form onSubmit={useFilter} className={error ? "formError" : ""} >
      {error && <label>{error}</label>}
      <search>
        <input
          placeholder="Ingrese un país"
          onChange={({ target: { value } }) => {
            setInput(value);
          }}
        />
        <button onClick={useFilter}>Buscar</button>
      </search>
    </form>
  );
};

export default FilterForm;
